package com.snail.cfg;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import ch.qos.logback.core.joran.spi.JoranException;
import com.woniu.utils.log.LogBackConfigLoader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class XLogger {

    private static boolean load = false;
    public final static org.slf4j.Logger logger = LoggerFactory.getLogger("snail");

    static {
        if (!load) {
            try { 
                LogBackConfigLoader.load("config/logback-log.xml");
                //LogBackConfigLoader.load(Jar.getPath(Xmonitor.class)+"/config/logback-log.xml");
            } catch (IOException | JoranException ex) {
                Logger.getLogger(XLogger.class.getName()).log(Level.SEVERE, null, ex);
            }
            load = true;
        }
    }
}
